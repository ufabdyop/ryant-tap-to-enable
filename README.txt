Tap to Enable
===

The python code used for monitoring USB card reader.

This code runs on debian (https://cloud.nanofab.utah.edu/index.php/s/bvn7wT4VQH5u4ls)

It could probably be adapted to run on ubuntu (https://cloud.nanofab.utah.edu/index.php/s/gHDIEflNwfIsGRe)

Installing on ubuntu 16.04
---
Some notes from setting this up on 2019-05-30 for nanofab.  Steve Dean and I worked through some issues that
were happening on a fresh install of ubuntu 16.04 from one of our custom install CDs.  The main issues were:

* the latest pyscard (v1.8 or so) was causing some cryptic errors.  To fix it, we used 'pip remove pyscard' and ran the
'python setup.py install' command to install it manually.

* cards appeared to not trigger an event.  To debug, I ran 'pcscd -fvd' in the foreground and pcsc_scan to discover there
was an issue with the driver.  We downloaded a newer version from omnikey (ifdokccid...4.0....tar.gz) and installed that.



Installing on ubuntu 14.04
---

Set up network:

/etc/network/interfaces:

		# This file describes the network interfaces available on your system
		# and how to activate them. For more information, see interfaces(5).

		# The loopback network interface
		auto lo
		iface lo inet loopback

		# The primary network interface
		auto eth0
		iface eth0 inet static
		address 155.98.92.144
		netmask 255.255.255.192
		gateway 155.98.92.129

/etc/resolv.conf:

		nameserver 8.8.8.8


Install Packages
---

Not sure which of these are strictly required, but ran the following:

		apt-get update; apt-get upgrade
		apt-get install pcscd pcsc-tools python-pyscard
		apt-get install build-essential autoconf libtool pkg-config python-opengl python-imaging python-pyrex python-pyside.qtopengl idle-python2.7 qt4-dev-tools qt4-designer libqtgui4 libqtcore4 libqt4-xml libqt4-test libqt4-script libqt4-network libqt4-dbus python-qt4 python-qt4-gl libgle3 python-dev
		apt-get install libpcsclite-dev python-pyscard python-pip git swig
		#pip install pyscard --upgrade ##removed -- later versions of pyscard seem to cause issues
		pip install requests
		git clone git@gitlab.eng.utah.edu:ryant/tap-to-enable.git
		cd tap-to-enable/
		cd pyscard-1.6.12
		python setup.py install
		cd ifdokrfid_lnx_i686-2.10.0.1 #note, may need a newer version of driver
		sudo ./install 

Set Up Auto-Login for TTY Tapper
---
sudo useradd -s /bin/bash -m 'tapper'
sudo visudo

		tapper  ALL=(root) NOPASSWD: /home/tapper/transmitobserver.py

sudo vim /etc/init/cardreader.conf 

		# cardreader - getty
		#
		# This service reads from HID cardreader
		# cardreader
		#

		start on (started networking and started pcscd)

		stop on runlevel [!2345]

		respawn

		exec su tapper -c '/home/tapper/transmitobserver.py' 



FIREWALL
---

Restrict firewall severely:

		sudo ufw enable
		for i in 155.98.11.63 155.98.11.22 8.8.8.8 155.98.110.48
		do
			sudo ufw allow in from $i
			sudo ufw allow out to $i
		done
		sudo ufw default deny outgoing
		sudo ufw default deny incoming

Nightly Restart
---
sudo crontab -e

		0	3	*	*	*	/sbin/reboot


Make Imageable
---
sudo rm /etc/udev/rules.d/70-persistent-net.rules





