from smartcard.CardMonitoring import CardMonitor, CardObserver
from smartcard.util import *
from smartcard.scard import *
from smartcard.CardType import ATRCardType
from smartcard.CardRequest import CardRequest
from time import sleep

class transmitobserver( CardObserver ):
    """A card observer that is notified when cards are inserted/removed from the system,
    connects to cards and SELECT DF_TELECOM
    """
    def __init__( self ):
        self.cards=[]

    def update( self, observable, (addedcards, removedcards) ):
        for card in addedcards:
            if card not in self.cards:
                self.cards+=[card]
                print "+Inserted: ", toHexString( card.atr )
                card.connection = card.createConnection()
                card.connection.connect()
		cardtype = ATRCardType( card.atr )
		SELECT = [0xFF, 0xCA, 0x00, 0x00, 0x00]
		apdu = SELECT
		cardrequest = CardRequest( timeout=1, cardType=cardtype )
                response, sw1, sw2 = card.connection.transmit( apdu )
                print "UID: ", toHexString( response )

        for card in removedcards:
            print "-Removed: ", toHexString( card.atr )
            if card in self.cards:
                self.cards.remove( card )


print "Insert or remove a smartcard in the system."
print ""
cardmonitor = CardMonitor()
cardobserver = transmitobserver()
cardmonitor.addObserver( cardobserver )

quit_key = raw_input("Type quit to quit: ")
while quit_key != "quit":
	quit_key = raw_input("Type quit to quit: ")	
