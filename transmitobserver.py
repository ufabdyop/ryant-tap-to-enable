#!/usr/bin/env python

from smartcard.CardMonitoring import CardMonitor, CardObserver
from smartcard.util import *
from smartcard.scard import *
from smartcard.CardType import ATRCardType
from smartcard.CardRequest import CardRequest
from time import sleep
import requests
import sys, pprint, datetime

quit_flag = False

class transmitobserver( CardObserver ):
    """A card observer that is notified when cards are inserted/removed from the system,
    connects to cards and SELECT DF_TELECOM
    """
    def __init__( self ):
        self.cards=[]

    def update( self, observable, (addedcards, removedcards) ):
        for card in addedcards:
            if card not in self.cards:
                self.cards+=[card]
                print datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                print "+Inserted: ", toHexString( card.atr )
                card.connection = card.createConnection()
                card.connection.connect()
                cardtype = ATRCardType( card.atr )
                SELECT = [0xFF, 0xCA, 0x00, 0x00, 0x00]
                apdu = SELECT
                cardrequest = CardRequest( timeout=1, cardType=cardtype )
                response, sw1, sw2 = card.connection.transmit( apdu )
                uid = toHexString(response).replace(' ', '')
                print "UID: ", uid
                url = 'http://coral.nanofab.utah.edu/lab/tap_to_enable/card_tapped/%s' % (uid,)
                print "Sending http request to %s" % (url,)
                try:
                        r = requests.get(url)
                except Exception as e:
                        print("Caught exception sending network request: %s" % pprint.pformat(e))
                        global quit_flag
                        quit_flag = True

        for card in removedcards:
            print "-Removed: ", toHexString( card.atr )
            if card in self.cards:
                self.cards.remove( card )

for i in range(10,0,-1):
	print("Sleeping %s seconds before starting" % i)
	sleep(1)

print "Insert or remove a smartcard in the system."
print ""
cardmonitor = CardMonitor()
cardobserver = transmitobserver()
cardmonitor.addObserver( cardobserver )

while True:
        sleep(2)
        if quit_flag:
                print("Quitting")
                break

